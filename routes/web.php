<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api-telegram-bot-lumen'], function () use ($router) {

    $router->post('/webhook-notificacoes-pedido/botUsername/{botUsername}', ['uses' => 'NotificacoesPedido@post']);
    $router->post('/send-message/botUsername/{botUsername}', ['uses' => 'SendMessage@post']);

    $router->post('/bot-cadastro', ['uses' => 'BotCadastro@post']);
    $router->post('/chat-cadastro', ['uses' => 'ChatCadastro@post']);

});