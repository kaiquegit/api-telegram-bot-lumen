<?php

namespace App\Tc\Posvenda\Chat;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response,
    App\Mirrors\Telegram\TelegramMirror;
use App\Models\TelegramBot;


class ValidaBot implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {

        $dadosTelegramBot = TelegramBot::where([
            ["token", "=", $data["token"]]
        ])
        ->get(["telegram_bot","informacoes_adicionais"])
        ->toArray();

        if (empty($dadosTelegramBot[0]["telegram_bot"])) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Bot',
                'detail' => 'Bot não cadastrado'
            ];

            $response = new Response($error, 406);

            return $response->response();

        } else {

            $data["telegram_bot"] = $dadosTelegramBot[0]["telegram_bot"];
            $arrayAdicionais      = json_decode($dadosTelegramBot[0]["informacoes_adicionais"], true);

        }

        $response = (new TelegramMirror)->postRequest($data["token"], 'getChatAdministrators', ["chat_id" => $data["chatId"]]);
        
        $botPossuiPrivilegios = false;
        foreach ($response["result"] as $key => $val) {

            if ($val["user"]["id"] == $arrayAdicionais["id"]) {

                $botPossuiPrivilegios = true;

            }

        }

        if (!$botPossuiPrivilegios) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Bot',
                'detail' => 'O Bot informado não tem privilégios suficientes no grupo'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }


        return $next->process($identifier, $data);
    }
}
