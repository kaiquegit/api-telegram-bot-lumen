<?php

namespace App\Tc\Posvenda\Chat;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response,
    App\Mirrors\Telegram\TelegramMirror;


class BuscaInfoChat implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {

        if (empty($data["chatId"])) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Chat',
                'detail' => 'Informe o ID do chat no telegram'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        if (empty($data["fabrica"])) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Fábrica',
                'detail' => 'Informe o ID da fabrica'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }
        
        $response = (new TelegramMirror)->postRequest($data["token"], 'getChat', ["chat_id" => $data["chatId"]]);

        if ($response["ok"]) {

            $data["chatInfo"] = $response["result"];

        } else {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Chat',
                'detail' => 'Chat não encontrado'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }
        
        return $next->process($identifier, $data);
    }
}
