<?php

namespace App\Tc\Posvenda\Chat;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\TelegramChat,
    App\Models\TelegramBot;
use App\Helpers\Funcoes;
use App\Mirrors\Telegram\TelegramMirror;


class ChatGravar implements RuleInterface
{

    use Funcoes;

    public function process(string $identifier, array $data, $next = null)
    {


    	$dadosTelegramChat = TelegramChat::where([
            ["chat_id", "=", $data["chatId"]],
            ["telegram_bot", "=", $data["telegram_bot"]]
        ])
        ->get(["telegram_chat"])
        ->toArray();

        $telegramChat = new TelegramChat();

        $label = "cadastrado";

        if (!empty($dadosTelegramChat[0]["telegram_chat"])) {

            $telegramChat = TelegramChat::find((int) $dadosTelegramChat[0]["telegram_chat"]);

            $telegramChat->fill(["telegram_chat" => (int) $dadosTelegramChat[0]["telegram_chat"]]);

            $label = "alterado";

        } else {

            $dadosTelegramChatValida = TelegramChat::where([
                ["telegram_bot", "=", $data["telegram_bot"]]
            ])
            ->get(["telegram_chat"])
            ->toArray();

            if (!empty($dadosTelegramChatValida[0]["telegram_chat"])) {

                $error = [
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title'  => 'Bot',
                    'detail' => 'O Bot informado já possui vínculo com outro grupo. Favor cadastrar um bot novo para prosseguir.'
                ];

                $response = new Response($error, 406);

                return $response->response();

            }

        }

        $telegramChat->tipo 					= $data['chatInfo']['type'];
        $telegramChat->fabrica   				= $data['fabrica'];
        $telegramChat->chat_id                  = $data['chatId'];
        $telegramChat->telegram_bot             = $data["telegram_bot"];
        $telegramChat->informacoes_adicionais	= $this->utf8Ansi(json_encode($data['chatInfo']));

        $telegramChat->save();

        (new TelegramMirror)->postRequest($data["token"], 'setChatDescription', [
            "chat_id" => (string) $data["chatId"],
            "description" => "Chat ID: ".$data["chatId"]
        ]);

        $content = [
            'meta' => [
                'result' => 'Chat '.$label.' com sucesso',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'telegram_chat',
                'id' => $telegramChat->telegram_chat
            ],
            'links' => [
                'self' => $_SERVER['REQUEST_URI']
            ]
        ];

        return $next->process($identifier, $content);
    }
}