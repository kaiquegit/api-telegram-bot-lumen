<?php

namespace App\Tc\Posvenda\Bot;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\TelegramBot;


class BotGravar implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        
    	$dadosTelegramBot = TelegramBot::where([
            ["token", "=", $data["token"]]
        ])
        ->get(["telegram_bot"])
        ->toArray();

        $telegramBot = new TelegramBot();

        $label = "cadastrado";

        if (!empty($dadosTelegramBot[0]["telegram_bot"])) {

            $telegramBot = TelegramBot::find((int) $dadosTelegramBot[0]["telegram_bot"]);

            $telegramBot->fill(["telegram_bot" => (int) $dadosTelegramBot[0]["telegram_bot"]]);

            $label = "alterado";

        }

        $telegramBot->codigo 					= $data['botInfo']['username'];
        $telegramBot->nome   					= $data['botInfo']['first_name'];
        $telegramBot->token  					= $data["token"];
        $telegramBot->informacoes_adicionais	= json_encode($data['botInfo']);

        $telegramBot->save();

        $content = [
            'meta' => [
                'result' => 'Bot '.$label.' com sucesso',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'telegram_bot',
                'id' => $telegramBot->telegram_bot
            ],
            'links' => [
                'self' => $_SERVER['REQUEST_URI']
            ]
        ];

        return $next->process($identifier, $content);
    }
}