<?php

namespace App\Tc\Posvenda\Message;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Mirrors\Telegram\TelegramMirror;
use App\Models\TelegramBot,
    App\Models\TelegramMessage;
use App\Helpers\Funcoes;


class EnviaMensagem implements RuleInterface
{

    use Funcoes;

    public function process(string $identifier, array $data, $next = null)
    {

        $response = (new TelegramMirror)->postRequest($data["token"], 'sendMessage', [
            "chat_id"    => $data["chat_id"],
            "text"       => $data["message"],
            "parse_mode" => "HTML"
        ]);

        if ($response["ok"]) {

            $telegramMessage = new TelegramMessage();

            $telegramMessage->telegram_chat          = $data["telegram_chat"];
            $telegramMessage->tipo                   = "automatico";
            $telegramMessage->mensagem               = $data["message"];
            $telegramMessage->informacoes_adicionais = $this->utf8Ansi(json_encode($response['result']));

            $telegramMessage->save();

        } else {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Message',
                'detail' => 'Falha ao enviar mensagem'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        $content = [
            'meta' => [
                'result' => 'Mensagem enviada com sucesso',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'telegram_message',
                'id' => $telegramMessage->telegram_message
            ],
            'links' => [
                'self' => $_SERVER['REQUEST_URI']
            ]
        ];

        return $next->process($identifier, $content);
    }
}