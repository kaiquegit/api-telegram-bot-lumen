<?php

namespace App\Tc\Posvenda\Message;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response,
    App\Mirrors\Telegram\TelegramMirror;
use App\Models\TelegramBot;


class ValidaMensagem implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {

        $dadosTelegramBot = TelegramBot::where([
            ["codigo", "=", $identifier]
        ])
        ->join("tbl_telegram_chat","tbl_telegram_chat.telegram_bot", "=", "tbl_telegram_bot.telegram_bot")
        ->get([
            "tbl_telegram_bot.token",
            "tbl_telegram_chat.chat_id",
            "tbl_telegram_chat.telegram_chat",
            "tbl_telegram_bot.telegram_bot"
        ])
        ->toArray();

        if (empty($dadosTelegramBot[0]["token"])) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Bot',
                'detail' => 'Bot ou chat não cadastrados'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        if (empty($data["message"])) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Mensagem',
                'detail' => 'Informe uma mensagem'
            ];

            $response = new Response($error, 406);

            return $response->response();


        }

        $data["token"]         = $dadosTelegramBot[0]["token"];
        $data["chat_id"]       = $dadosTelegramBot[0]["chat_id"];
        $data["telegram_chat"] = $dadosTelegramBot[0]["telegram_chat"];
        $data["telegram_bot"]  = $dadosTelegramBot[0]["telegram_bot"];

        return $next->process($identifier, $data);
    }
}
