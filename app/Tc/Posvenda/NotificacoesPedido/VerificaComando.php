<?php

namespace App\Tc\Posvenda\NotificacoesPedido;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response,
    App\Mirrors\Telegram\TelegramMirror;


class VerificaComando implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {

        if (strrpos($data["message"]["text"], "/pedido") !== false) {

            $data["pedido_id"] = trim(explode(" ", $data["message"]["text"])[1]);

            $next = new \App\Tc\Posvenda\NotificacoesPedido\ConsultaPedido;

        } else {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Comando',
                'detail' => 'Comando não configurado'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }
        
        return $next->process($identifier, $data);
    }
}
