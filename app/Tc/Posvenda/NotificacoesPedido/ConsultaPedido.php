<?php

namespace App\Tc\Posvenda\NotificacoesPedido;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\Pedido,
    App\Models\Fabrica,
    App\Models\Posto,
    App\Models\TelegramBot;
use App\Helpers\Funcoes;
use App\Mirrors\Telegram\TelegramMirror;


class ConsultaPedido implements RuleInterface
{

    use Funcoes;

    public function process(string $identifier, array $data, $next = null)
    {

        $dadosPedido = Pedido::where([
            ["tbl_pedido.pedido", "=", $data["pedido_id"]]
        ])
        ->join("tbl_fabrica", "tbl_fabrica.fabrica", "=", "tbl_pedido.fabrica")
        ->join("tbl_posto", "tbl_posto.posto", "=", "tbl_pedido.posto")
        ->get([
            "tbl_fabrica.nome as nome_fabrica",
            "tbl_pedido.total",
            "tbl_posto.nome as nome_posto",
            "tbl_pedido.data as data"
        ])
        ->toArray();

        if (count($dadosPedido) > 0) {

            $mensagem = "<b>Pedido</b> ".$data["pedido_id"]." - ".$dadosPedido[0]["nome_fabrica"]."\n\n<b>Posto:</b> ".$dadosPedido[0]["nome_posto"]."\n<b>Total:</b> ".$dadosPedido[0]["total"]."\n<b>Data:</b> ".$this->mostraData($dadosPedido[0]["data"])."\n<a href=\"https://posvenda.telecontrol.com.br/assist/admin/pedido_admin_consulta.php?pedido=".$data["pedido_id"]."\">Visualizar Pedido </a> (necessário estar logado na fábrica)";

        } else {

            $mensagem = "Não encontramos um pedido com esse número (".$data["pedido_id"].")";

        }

        $dadosTelegramBot = TelegramBot::where([
            ["codigo", "=", $identifier]
        ])
        ->get(["token","telegram_bot"])
        ->toArray();

        if (!empty($dadosTelegramBot[0]["token"])) {

            (new TelegramMirror)->postRequest($dadosTelegramBot[0]["token"], 'sendMessage', [
                "chat_id" => (string) $data["message"]["chat"]["id"],
                "text" => $mensagem,
                "parse_mode" => "HTML"
            ]);

        }

        $content = [
            'meta' => [
                'result' => 'Mensagem enviada',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'telegram_bot',
                'id' => $dadosTelegramBot[0]["telegram_bot"]
            ],
            'links' => [
                'self' => $_SERVER['REQUEST_URI']
            ]
        ];

        $response = new Response($content, 201);

        return $response->response();

    }
}