<?php

namespace App\Tc\Posvenda\ChatMigrate;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\TelegramChat,
    App\Models\TelegramBot;
use App\Helpers\Funcoes;
use App\Mirrors\Telegram\TelegramMirror;


class AtualizaChat implements RuleInterface
{

    use Funcoes;

    public function process(string $identifier, array $data, $next = null)
    {

        $telegramChat = new TelegramChat();

        $telegramChat = TelegramChat::find((int) $data["telegram_chat"]);

        $telegramChat->fill(["telegram_chat" => (int) $data["telegram_chat"]]);

        $telegramChat->tipo 					= $data['chatInfo']['type'];
        $telegramChat->chat_id                  = $data['chat_id_novo'];
        $telegramChat->informacoes_adicionais	= $this->utf8Ansi(json_encode($data['chatInfo']));

        $telegramChat->save();

        (new TelegramMirror)->postRequest($data["token"], 'setChatDescription', [
            "chat_id" => (string) $data["chat_id_novo"],
            "description" => "Chat ID: ".$data["chat_id_novo"]
        ]);

        $content = [
            'meta' => [
                'result' => 'Chat atualizado com sucesso',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'telegram_chat',
                'id' => $telegramChat->telegram_chat
            ],
            'links' => [
                'self' => $_SERVER['REQUEST_URI']
            ]
        ];

        return $next->process($identifier, $content);
    }
}