<?php

namespace App\Tc\Posvenda\ChatMigrate;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response,
    App\Mirrors\Telegram\TelegramMirror;
use App\Models\TelegramChat,
    App\Models\TelegramBot;


class AtualizaInfoChat implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {

        $chatIdAntigo = $data["message"]["chat"]["id"];
        $chatIdNovo   = $data["message"]["migrate_to_chat_id"];

        $dadosChat = TelegramChat::where([
            ["chat_id", "=", $chatIdAntigo]
        ])
        ->get(["telegram_chat"])
        ->toArray();

        if (!empty($dadosChat[0]["telegram_chat"])) {

            $dadosTelegramBot = TelegramBot::where([
                ["codigo", "=", $data["botUsername"]]
            ])
            ->get(["token"])
            ->toArray();

            $response = (new TelegramMirror)->postRequest($dadosTelegramBot[0]["token"], 'getChat', ["chat_id" => $chatIdNovo]);

            if ($response["ok"]) {

                $data["chatInfo"]      = $response["result"];
                $data["telegram_chat"] = $dadosChat[0]["telegram_chat"];
                $data["token"]         = $dadosTelegramBot[0]["token"];
                $data["chat_id_novo"]  = $chatIdNovo;

            } else {

                $error = [
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title'  => 'Chat',
                    'detail' => 'Chat não encontrado'
                ];

                $response = new Response($error, 406);

                return $response->response();

            }

        } else {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Chat',
                'detail' => 'Chat não encontrado'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }
        
        return $next->process($identifier, $data);
    }
}
