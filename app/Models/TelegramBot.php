<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TelegramBot extends Model
{
    protected $table = 'tbl_telegram_bot';
    protected $primaryKey = 'telegram_bot';
    public $timestamps = false;

    protected $fillable = [
       "telegram_bot"
    ];

}
