<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Pedido extends Model
{
    protected $table = 'tbl_pedido';
    protected $primaryKey = 'pedido';
    public $timestamps = false;

    protected $fillable = [
       "pedido"
    ];

}
