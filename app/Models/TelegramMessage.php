<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TelegramMessage extends Model
{
    protected $table = 'tbl_telegram_message';
    protected $primaryKey = 'telegram_message';
    public $timestamps = false;

    protected $fillable = [
       "telegram_message"
    ];

}
