<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Posto extends Model
{
    protected $table = 'tbl_posto';
    protected $primaryKey = 'posto';
    public $timestamps = false;

    protected $fillable = [
       "posto"
    ];

}
