<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TelegramChat extends Model
{
    protected $table = 'tbl_telegram_chat';
    protected $primaryKey = 'telegram_chat';
    public $timestamps = false;

    protected $fillable = [
       "telegram_chat"
    ];

}
