<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue,
    App\Tc\Regras\RuleEngine\Response;


class NotificacoesPedido extends BaseController
{


    public function post(Request $request, $botUsername)
    {
        $data = $request->all();

        if (empty($botUsername)) {

            $error = [
                'source' => ['pointer' => '/' . $request->path()],
                'title' => 'botUsername Obrigatório',
                'detail' => 'Informe o nome de usuário do bot'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        if (!empty($data["message"]["migrate_to_chat_id"])) {

            $regra      = "chatMigrate";
            $identifier = "";
            $data["botUsername"] = $botUsername;

        } else if ($data["message"]["entities"][0]["type"] == "bot_command") {

            $regra      = "notificacoesPedido";
            $identifier = $botUsername;

        } else {

            exit;

        }

        $loader = new RulesLoader($identifier, 'webhook/'.$regra);
        $queue  = new Queue($loader->getRules());

        return $queue->run($identifier, $data);
    }
}
