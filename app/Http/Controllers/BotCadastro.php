<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue,
    App\Tc\Regras\RuleEngine\Response;


class BotCadastro extends BaseController
{


    public function post(Request $request)
    {
        $data = $request->all();

        $botToken = $request->headers->get('bot-token');

        if (empty($botToken)) {

            $error = [
                'source' => ['pointer' => '/' . $request->path()],
                'title' => 'Token',
                'detail' => 'Informe a token do bot nos headers'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        $data["token"] = $botToken;

        $identifier = 'default';

        $loader = new RulesLoader($identifier, 'gravar/bot');
        $queue = new Queue($loader->getRules());

        return $queue->run($identifier, $data);
    }
}
