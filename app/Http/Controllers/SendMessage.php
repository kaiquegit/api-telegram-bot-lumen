<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue,
    App\Tc\Regras\RuleEngine\Response;


class SendMessage extends BaseController
{


    public function post(Request $request, $botUsername)
    {
        $data = $request->all();

        if (empty($botUsername)) {

            $error = [
                'source' => ['pointer' => '/' . $request->path()],
                'title'  => 'Username',
                'detail' => 'Informe o username do bot'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        $identifier = $botUsername;

        $loader = new RulesLoader($identifier, 'gravar/message');
        $queue = new Queue($loader->getRules());

        return $queue->run($identifier, $data);
    }
}
