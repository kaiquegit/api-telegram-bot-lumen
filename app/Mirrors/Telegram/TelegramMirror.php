<?php
namespace App\Mirrors\Telegram;

use App\Tc\Regras\RuleEngine\Response;

class TelegramMirror
{

    public function getRequest($token, $method)
    {
        
        $client = new \GuzzleHttp\Client();

        if (empty($token) || empty($method)) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Token',
                'detail' => 'Informe a token e método'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        $retorno = $client->request('GET', 'https://api.telegram.org/bot'.$token.'/'.$method)->getBody();

        return json_decode($retorno, true);

    }

    public function postRequest($token, $method, $body = [])
    {
        
        $client = new \GuzzleHttp\Client();

        if (empty($token) || empty($method)) {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title'  => 'Token',
                'detail' => 'Informe a token e método'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }

        $retorno = $client->request('POST', 'https://api.telegram.org/bot'.$token.'/'.$method, ['form_params' => $body])->getBody();

        return json_decode($retorno, true);

    }

}
