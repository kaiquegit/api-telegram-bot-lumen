<?php

namespace App\Tc\Posvenda\NotificacoesPedido;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response,
    App\Mirrors\Telegram\TelegramMirror;


class VerificaAtualizacoes implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        
        $response = (new TelegramMirror)->getRequest($data["token"], 'getMe');

        if ($response["ok"]) {

            $data["botInfo"] = $response["result"];

        } else {

            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Bot',
                'detail' => 'Bot não encontrado'
            ];

            $response = new Response($error, 406);

            return $response->response();

        }
        
        return $next->process($identifier, $data);
    }
}
