<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue,
    App\Tc\Regras\RuleEngine\Response;


class OsItemController extends BaseController
{
    public function post(Request $request)
    {
        $data = $request->all();

        $required = ['fabrica', 'os', 'peca_referencia', 'qtde', 'servico_realizado'];

        foreach ($required as $req) {
            if (empty($data[$req])) {
                $error = [
                    'source' => ['pointer' => '/' . $request->path()],
                    'title' => 'Campo obrigatório',
                    'detail' => $req
                ];

                $response = new Response($error, 406);

                return $response->response();
            }
        }

        $loader = new RulesLoader($data['fabrica'], 'gravar/item');
        $queue = new Queue($loader->getRules());

        return $queue->run($data['fabrica'], $data);
    }
}
