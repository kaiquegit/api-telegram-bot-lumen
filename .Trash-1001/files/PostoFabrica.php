<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PostoFabrica extends Model
{
    protected $table = 'tbl_posto_fabrica';
    protected $primaryKey = 'posto_fabrica';
    public $timestamps = false;
}
