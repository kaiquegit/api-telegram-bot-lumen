<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OsItem extends Model
{
    protected $table = 'tbl_os_item';
    protected $primaryKey = 'os_item';
    public $timestamps = false;
}
