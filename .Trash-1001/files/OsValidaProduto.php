<?php

namespace App\Tc\Posvenda;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\Produto;


class OsValidaProduto implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $produto = Produto::where('referencia', '=', $data['produto_referencia'])
            ->where('fabrica_i', '=', $identifier)
            ->first();

        if (empty($produto)) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Produto não encontrado',
                'detail' => 'Produto não encontrado'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        unset($data['produto_referencia']);
        $data['produto'] = $produto->produto;

        return $next->process($identifier, $data);
    }
}
