<?php

namespace App\Tc\Posvenda;

use App\Tc\Regras\RuleEngine\RuleInterface;


class OsValidaGarantia implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        return $next->process($identifier, $data);
    }
}
