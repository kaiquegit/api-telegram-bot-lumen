<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue,
    App\Tc\Regras\RuleEngine\Response;


class OsController extends BaseController
{
    public function post(Request $request)
    {
        $data = $request->all();

        if (empty($data['fabrica'])) {
            $error = [
                'source' => ['pointer' => '/' . $request->path()],
                'title' => 'Campo obrigatório',
                'detail' => 'fabrica'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        $loader = new RulesLoader($data['fabrica'], 'gravar/os');
        $queue = new Queue($loader->getRules());

        return $queue->run($data['fabrica'], $data);
    }
}
