<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OsExtra extends Model
{
    protected $table = 'tbl_os_extra';
    protected $primaryKey = 'os';
    public $timestamps = false;
}
