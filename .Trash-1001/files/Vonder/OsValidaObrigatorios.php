<?php

namespace App\Tc\Posvenda\Vonder;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Tc\Posvenda\CamposObrigatorios\Vonder as CamposObrigatorios;


class OsValidaObrigatorios implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $obrigatorios = CamposObrigatorios::getCamposObrigatorios();

        foreach ($obrigatorios as $obr) {
            $key = $obr['key'];
            $type = $obr['type'];
            $attr = (array_key_exists('attributes', $obr)) ? $obr['attributes'] : [];

            if (!array_key_exists($key, $data) or empty($data[$key])) {
                $error = [
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title' => 'Campo obrigatório',
                    'detail' => $obr
                ];

                $response = new Response($error, 406);

                return $response->response();
            }

            if (in_array($type, ['string', 'numeric']) and array_key_exists('max_length', $attr)) {
                if (strlen($data[$key]) > $attr['max_length']) {
                    $error = [
                        'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                        'title' => 'Tamanho máximo de campo excedido',
                        'detail' => $obr
                    ];
                    
                    $response = new Response($error, 406);

                    return $response->response();
                }
            }

            if ($type == 'date' and array_key_exists('format', $attr)) {
                $src = ['YYYY', 'MM', 'DD'];
                $rep = ['Y', 'm', 'd'];
                $format = str_replace($src, $rep, $attr['format']);

                $date = \DateTime::createFromFormat($format, $data[$key]);

                if (empty($date) or ($date->format($format) <> $data[$key])) {
                    $error = [
                        'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                        'title' => 'Data inválida',
                        'detail' => $obr
                    ];
                    
                    $response = new Response($error, 406);

                    return $response->response();
                }
            }
        }

        return $next->process($identifier, $data);
    }
}
