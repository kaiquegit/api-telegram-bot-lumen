<?php

namespace App\Tc\Posvenda\Vonder;

use App\Tc\Regras\RuleEngine\RuleInterface;
use App\Models\Os,
    App\Models\OsExtra,
    App\Models\OsProduto;
use Illuminate\Support\Facades\DB;


class OsGravar implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $gravar = $data;

        $toUnset = [
            'revenda_cep',
            'revenda_endereco',
            'revenda_numero',
            'revenda_bairro',
            'revenda_cidade',
            'revenda_estado',
            'defeito_reclamado_descricao',
            'data_recebimento_produto'
        ];

        foreach ($toUnset as $val) {
            unset($gravar[$val]);
        }

        $toIso = [
            'consumidor_nome',
            'consumidor_endereco',
            'consumidor_bairro',
            'consumidor_cidade'
        ];

        foreach ($toIso as $key) {
            $gravar[$key] = utf8_decode($gravar[$key]);
        }

        $defeito = DB::select('SELECT UPPER(UNACCENT(?)) AS reclamado_descricao', [$data['defeito_reclamado_descricao']]);

        $os = new Os();

        foreach ($gravar as $key => $val) {
            $os->$key = $val;
        }

        $os->defeito_reclamado_descricao = $defeito[0]->reclamado_descricao;
        $os->consumidor_revenda = 'C';

        $os->save();

        $os->os_numero = $os->os;
        $os->sua_os = $os->os;

        $osExtra = new osExtra();
        $osExtra->os = $os->os;
        $osExtra->i_fabrica = $identifier;

        $osProduto = new OsProduto();
        $osProduto->os = $os->os;
        $osProduto->produto = $os->produto;

        $os->save();
        $osExtra->save();
        $osProduto->save();

        $content = [
            'meta' => [
                'result' => 'sucess',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'os',
                'id' => $os->os
            ],
            'links' => [
                'self' => '/api-os/os/' . $os->os
            ]
        ];

        return $next->process($identifier, $content);
    }
}
