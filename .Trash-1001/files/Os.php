<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Os extends Model
{
    protected $table = 'tbl_os';
    protected $primaryKey = 'os';
    public $timestamps = false;
}
