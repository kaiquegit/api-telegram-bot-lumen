<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Peca extends Model
{
    protected $table = 'tbl_peca';
    protected $primaryKey = 'peca';
    public $timestamps = false;
}
