<?php

namespace App\Tc\Posvenda;

use App\Tc\Regras\RuleEngine\RuleInterface;
use App\Models\PostoFabrica,
    App\Models\Revenda,
    App\Models\Cidade;


class OsValidaRevenda implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $revenda = Revenda::where('cnpj', '=', $data['revenda_cnpj'])->first();

        if (empty($revenda)) {
            $cidade = Cidade::whereRaw('nome = UPPER(UNACCENT(?))', [$data['revenda_cidade']])
                ->whereRaw('estado = UNACCENT(?)', [$data['revenda_estado']])
                ->whereRaw("pais = 'BR'")
                ->first();

            if (empty($cidade)) {
                $error = [
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title' => 'Não foi possível completar a requisição',
                    'detail' => ['revenda_cidade', 'revenda_estado']
                ];

                $response = new Response($error, 406);

                return $response->response();
            }
            
            $revenda = new Revenda();
            $revenda->nome = utf8_decode($data['revenda_nome']);
            $revenda->endereco = $data['revenda_endereco'];
            $revenda->numero = $data['revenda_numero'];
            //$revenda->complemento = $data['revenda_complemento'];
            $revenda->bairro = utf8_decode($data['revenda_bairro']);
            $revenda->cep = $data['revenda_cep'];
            $revenda->cidade = utf8_decode($data['revenda_cidade']);
            $revenda->cnpj = $data['revenda_cnpj'];
            $revenda->fone = $data['revenda_fone'];
            $revenda->save();
        }

        $data['revenda'] = $revenda->revenda;

        return $next->process($identifier, $data);
    }
}
