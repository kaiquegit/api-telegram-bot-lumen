<?php

namespace App\Tc\Posvenda\Item;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\Peca;


class ValidaPeca implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $peca = Peca::where('referencia', '=', $data['peca_referencia'])
                    ->where('fabrica', '=', $identifier)
                    ->first();

        if (empty($peca)) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Peça não encontrada',
                'detail' => 'Peça não encontrada'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        unset($data['peca_referencia']);
        $data['peca'] = $peca->peca;

        return $next->process($identifier, $data);
    }
}
