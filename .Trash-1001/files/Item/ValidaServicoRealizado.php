<?php

namespace App\Tc\Posvenda\Item;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\ServicoRealizado;


class ValidaServicoRealizado implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $servicoRealizado = ServicoRealizado::where('fabrica', '=', $identifier)
                                            ->where('descricao', '=', utf8_decode($data['servico_realizado']))
                                            ->where('ativo', '=', 't')
                                            ->first();

        if (empty($servicoRealizado)) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Serviço realizado não encontrado',
                'detail' => 'Serviço realiazado não encontrado'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        $data['servico_realizado'] = $servicoRealizado->servico_realizado;

        return $next->process($identifier, $data);
    }
}
