<?php

namespace App\Tc\Posvenda\Item;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\Os,
    App\Models\ListaBasica;


class ValidaListaBasica implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $os = Os::where('os', '=', $data['os'])
                        ->where('fabrica', '=', $identifier)
                        ->first();

        if (empty($os)) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'OS não encontrada',
                'detail' => 'OS não encontrada'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        $listaBasica = ListaBasica::where('produto', '=', $os->produto)
                                    ->where('peca', '=', $data['peca'])
                                    ->first();

        if (empty($listaBasica)) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Lista básica de produto não encontrada',
                'detail' => 'Lista básica de produto não encontrada'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        if ((int) $data['qtde'] > (int) $listaBasica->qtde) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Quantidade solicitada maior que a permitida',
                'detail' => 'Quantidade solicitada maior que a permitida'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        $data['produto'] = $os->produto;

        return $next->process($identifier, $data);
    }
}
