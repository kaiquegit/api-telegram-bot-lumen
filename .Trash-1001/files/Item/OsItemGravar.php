<?php

namespace App\Tc\Posvenda\Item;

use App\Tc\Regras\RuleEngine\RuleInterface;
use App\Models\OsProduto,
    App\Models\OsItem;


class OsItemGravar implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $osProduto = OsProduto::where('os', '=', $data['os'])->first();

        if (empty($osProduto)) {
            $osProduto = new OsProduto();
            $osProduto->os = $data['os'];
            $osProduto->produto = $data['produto'];
            $osProduto->save();
        }

        $osItem = new OsItem();
        $osItem->os_produto = $osProduto->os_produto;
        $osItem->peca = $data['peca'];
        $osItem->qtde = $data['qtde'];
        $osItem->servico_realizado = $data['servico_realizado'];
        $osItem->save();

        $content = [
            'meta' => [
                'result' => 'sucess',
                'status'=> 201
            ], 
            'data' => [
                'type' => 'os',
                'id' => $osProduto->os
            ],
            'links' => [
                'self' => '/api-os/os/' . $osProduto->os
            ]
        ];

        return $next->process($identifier, $content);
    }
}
