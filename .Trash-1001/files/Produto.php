<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Produto extends Model
{
    protected $table = 'tbl_produto';
    protected $primaryKey = 'produto';
    public $timestamps = false;
}
