<?php

namespace App\Tc\Posvenda\CamposObrigatorios;

use App\Tc\Regras\RuleEngine\RuleInterface;


class CamposObrigatorios implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        return $next->process($identifier, $data);
    }
}
