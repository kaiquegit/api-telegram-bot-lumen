<?php

namespace App\Tc\Posvenda\CamposObrigatorios;

use App\Tc\Regras\RuleEngine\RuleInterface;


class Vonder implements RuleInterface
{
    private static $camposObrigatorios = [
        ['key' => 'codigo_posto', 'type' => 'string', 'attributes' => ['max_length' => 20]],
        ['key' => 'data_abertura', 'type' => 'date', 'attributes' => ['format' => 'YYYY-MM-DD']],
        ['key' => 'produto_referencia', 'type' => 'string', 'attributes' => ['max_length' => 30]],
        ['key' => 'consumidor_nome', 'type' => 'string', 'attributes' => ['max_length' => 50]],
        ['key' => 'consumidor_fone', 'type' => 'numeric', 'attributes' => ['max_length' => 14]],
        ['key' => 'consumidor_cep', 'type' => 'numeric', 'attributes' => ['max_length' => 8]],
        ['key' => 'consumidor_endereco', 'type' => 'text'],
        ['key' => 'consumidor_numero', 'type' => 'string', 'attributes' => ['max_length' => 10]],
        ['key' => 'consumidor_bairro', 'type' => 'string', 'attributes' => ['max_length' => 80]],
        ['key' => 'consumidor_cidade', 'type' => 'string', 'attributes' => ['max_length' => 70]],
        ['key' => 'consumidor_estado', 'type' => 'string', 'attributes' => ['max_length' => 2]],
        ['key' => 'nota_fiscal', 'type' => 'string', 'attributes' => ['max_length' => 20]],
        ['key' => 'data_nf', 'type' => 'date', 'attributes' => ['format' => 'YYYY-MM-DD']],
        ['key' => 'defeito_reclamado_descricao', 'type' => 'text'],
        ['key' => 'revenda_nome', 'type' => 'string', 'attributes' => ['max_length' => 50]],
        ['key' => 'revenda_cnpj', 'type' => 'numeric', 'attributes' => ['max_length' => 14]],
        ['key' => 'revenda_fone', 'type' => 'numeric', 'attributes' => ['max_length' => 14]],
        ['key' => 'revenda_cep', 'type' => 'numeric', 'attributes' => ['max_length' => 8]],
        ['key' => 'revenda_endereco', 'type' => 'string', 'attributes' => ['max_length' => 60]],
        ['key' => 'revenda_numero', 'type' => 'string', 'attributes' => ['max_length' => 10]],
        ['key' => 'revenda_bairro', 'type' => 'string', 'attributes' => ['max_length' => 80]],
        ['key' => 'revenda_cidade', 'type' => 'string', 'attributes' => ['max_length' => 70]],
        ['key' => 'revenda_estado', 'type' => 'string', 'attributes' => ['max_length' => 2]],
        ['key' => 'aparencia_produto', 'type' => 'text'],
        ['key' => 'data_recebimento_produto', 'type' => 'date', 'attributes' => ['format' => 'YYYY-MM-DD']],
    ];
    
    public function process(string $identifier, array $data, $next = null)
    {
        $obrigatorios = self::$camposObrigatorios;

        $data['data'] = [
            'fabrica' => $identifier,
            'campos_obrigatorios' => $obrigatorios
        ];

        $data['results'] = count($obrigatorios);
        $data['pages'] = 1;
        $data['perPage'] = count($obrigatorios);
        $data['current'] = 1;

        $data['type'] = 'campos_obrigatorios';
        $data['id'] = $identifier;

        $data['links'] = ['self' => '/api-os/campos_obrigatorios/' . $identifier];

        return $next->process($identifier, $data);
    }

    public static function getCamposObrigatorios()
    {
        return self::$camposObrigatorios;
    }
}
