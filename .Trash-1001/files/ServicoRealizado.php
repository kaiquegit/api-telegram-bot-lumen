<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ServicoRealizado extends Model
{
    protected $table = 'tbl_servico_realizado';
    protected $primaryKey = 'servico_realizado';
    public $timestamps = false;
}
