<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ListaBasica extends Model
{
    protected $table = 'tbl_lista_basica';
    protected $primaryKey = 'lista_basica';
    public $timestamps = false;
}
