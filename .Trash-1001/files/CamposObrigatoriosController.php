<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue;


class CamposObrigatoriosController extends BaseController
{
    public function get($fabrica)
    {
        $loader = new RulesLoader($fabrica, 'campos_obrigatorios');
        $queue = new Queue($loader->getRules());

        return $queue->run($fabrica, []);
    }
}
