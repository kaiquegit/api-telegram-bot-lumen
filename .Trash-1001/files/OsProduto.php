<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OsProduto extends Model
{
    protected $table = 'tbl_os_produto';
    protected $primaryKey = 'os_produto';
    public $timestamps = false;
}
