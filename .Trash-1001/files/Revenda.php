<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Revenda extends Model
{
    protected $table = 'tbl_revenda';
    protected $primaryKey = 'revenda';
    public $timestamps = false;
}
