<?php

namespace App\Tc\Posvenda;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\PostoFabrica;


class OsValidaCodigoPosto implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $posto = PostoFabrica::where('codigo_posto', '=', $data['codigo_posto'])
            ->where('fabrica', '=', $identifier)
            ->first();

        if (empty($posto)) {
            $error = [
                'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                'title' => 'Posto não encontrado',
                'detail' => 'Posto não encontrado'
            ];

            $response = new Response($error, 406);

            return $response->response();
        }

        unset($data['codigo_posto']);
        $data['posto'] = $posto->posto;

        return $next->process($identifier, $data);
    }
}
